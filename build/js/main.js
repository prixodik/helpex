"use strict";
(function($){

if( $(".js-slick-img").length ){
	
	/*$(".js-slick-img").slick({
		autoplay: false, 
		adaptiveHeight: true, 
		dots: false,
		arrows: true,
		infinite: true,
		speed: 3000,
		slidesToShow: 3, 
		slidesToScroll: 1,
		centerMode: true,
		//edgeFriction: 1,
		waitForAnimate: true,
		centerPadding: '60px',
		asNavFor: $(".js-slick-info"),
		focusOnSelect: true,
		prevArrow: $('.products-slider__prev'),
		nextArrow: $('.products-slider__next')
	});

	$('.js-slick-img').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		console.log(nextSlide);
	});

	$(".js-slick-info").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: $(".js-slick-img"),
		dots: true,
		arrows: false,
		vertical: false,
		centerMode: false,
		fade: true,
		cssEase: 'linear'
	});*/

	$(".js-slick-img").carousel({
	
		hAlign: 'center',
		vAlign: 'bottom',
		hMargin: 1,
		vMargin: 0.3,
		frontWidth: 500,
		frontHeight: 360,
		carouselWidth: $(".js-slick-img").width(),
		carouselHeight: 360,
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		backZoom: 0.8,
		slidesPerScroll: 3,
		speed: 700,
		buttonNav: 'bullets',
		directionNav: true,
		autoplay: false,
		autoplayInterval: 3000,
		pauseOnHover: true,
		mouse: true,
		shadow: false,
		reflection: false,
		//reflectionHeight: 0.2,
		//reflectionOpacity: 0.5,
		//reflectionColor: '255,255,255',
		description: true,
		descriptionContainer: '.js-slick-info',
		backOpacity: 1,
		before: function(carousel) {
			//console.log(carousel);
			//$('.js-slick-img .slick-slide').eq(carousel.goTo()).hide();
			$('.js-slick-img .slick-slide').eq(carousel.current).removeClass("slick-current");
			$('.slick-slide .slick-slide__bg').css("background", "#e8e8e8");
			
		},
		after: function(carousel) {
			$('.js-slick-img .slick-slide').eq(carousel.current).addClass("slick-current");
			var color = $('.js-slick-img .slick-slide').eq(carousel.current).data("color");
			console.log(color);

			$(".products-slider__prev").css("background", color);
			$(".products-slider__next").css("background", color);
			$('.slick-current .slick-slide__bg').css("background", color);
			$('.products-slider__title').css("color", color);
			$('.products-slider__more').css("color", color);

			$('.bullet:not(.bulletActive)').css("border-color", color).css("background",color);
			$('.bulletActive').css("border-color", color).css("background","#fff");
		}
	});

	$(".products-slider__next").click(function(){
		$(".nextButton").trigger("click");
		return false;
	});
	$(".products-slider__prev").click(function(){
		$(".prevButton").trigger("click");
		return false;
	});

}


$(document).ready(function(){ 
	


});
})(jQuery)